
title          = "The Author"
category       = ""
content        = [[My name is Joan Arnaldich. I am a software developer presently working in Barcelona, Spain.

My main interest areas in computing are non-imperative programming paradigms, parallel computing, image processing, non-mainstream languages and, to some extent, web development.

I am currently *not* looking for a job.]]
breadcrumb     = ""