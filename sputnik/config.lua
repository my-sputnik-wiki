
fields         = ""
title          = "Configurations"
category       = ""
actions        = ""
config         = ""
markup_module  = ""
templates      = ""
translations   = ""
prototype      = "sputnik/config_defaults"
permissions    = ""
html_main      = ""
html_head      = ""
html_menu      = ""
html_logo      = ""
html_search    = ""
html_page      = ""
html_content   = ""
html_body      = ""
html_footer    = ""
html_header    = ""
html_sidebar   = ""
html_meta_keywords= ""
html_meta_description= ""
redirect_destination= ""
xssfilter_allowed_tags= ""
http_cache_control= ""
http_expires   = ""
content        = [[SITE_TITLE     = "Rubber Bytes - Joan Arnaldich Home Page"        -- change the title of the site
DOMAIN         = "rubberbytes.nfshost.com"     -- set for RSS feeds to work properly
NICE_URL       = BASE_URL.."?p="      -- set if you want "nicer" URLs
MAIN_COLOR     = 200                  -- pick a number from 0 to 360
--BODY_BG_COLOR  = "white"

HOME_PAGE      = "index"
HOME_PAGE_URL  = NICE_URL             -- or NICE_URL.."?p="..HOME_PAGE
COOKIE_NAME    = "Sputnik"            -- change if you run several

--SEARCH_CONTENT = "Installation"

TIME_ZONE      = "+0000"
TIME_ZONE_NAME = "<abbr title='Greenwich Mean Time'>GMT</abbr>"


INTERFACE_LANGUAGE = "en"

]]
edit_ui        = ""
admin_edit_ui  = ""
breadcrumb     = ""
save_hook      = ""
