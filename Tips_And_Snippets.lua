
fields         = ""
title          = "Tips & Snippets"
category       = ""
actions        = ""
config         = ""
markup_module  = ""
templates      = ""
translations   = ""
permissions    = ""
html_main      = ""
html_head      = ""
html_menu      = ""
html_logo      = ""
html_search    = ""
html_page      = ""
html_content   = ""
html_body      = ""
html_header    = ""
html_footer    = ""
html_sidebar   = ""
html_meta_keywords= ""
html_meta_description= ""
redirect_destination= ""
xssfilter_allowed_tags= ""
http_cache_control= ""
http_expires   = ""
content        = [[Remember we said the representation format for a wiki page is itself valid lua code. This lets you manipulate programmatically the nodes in the Wiki to achieve different effects. Here's an example.

Exporting an Image
------------------

This snippet shows how to export the contents of an image node containing a .jpg file to disk. This might be an ugly hack, but it works...

	require"node.lua"
	require"base64"
	out = assert(io.open("node.jpg", "wb"))
	out:write(base64.decode(content))
	out:close()

]]
edit_ui        = ""
admin_edit_ui  = ""
breadcrumb     = "Tips"
save_hook      = ""