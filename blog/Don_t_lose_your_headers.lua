
fields         = ""
title          = "blog/Don't lose your headers"
category       = ""
actions        = ""
config         = ""
markup_module  = ""
templates      = ""
translations   = ""
prototype      = "@BlogEntry"
permissions    = ""
html_main      = ""
html_head      = ""
html_menu      = ""
html_logo      = ""
html_search    = ""
html_page      = ""
html_content   = ""
html_body      = ""
html_footer    = ""
html_header    = ""
html_sidebar   = ""
html_meta_keywords= "headers, http, web, tutorial"
html_meta_description= "Some useful http headers explained"
redirect_destination= ""
xssfilter_allowed_tags= ""
http_cache_control= ""
http_expires   = ""
content        = [[Most of the times programmers are so concerned about building the
content part of the response that headers are left to the
default. This is probably because clients and web frameworks do their
best to understand each other and assume sensible defaults. There are
times, though, when this proves not enough: as a web user, too often I
find pages with bugs due to inconsistent headers, so I thought I would
list here some of what I think are the most common header mistakes, if
only as a reminder to an eventual webdev me.

## Charset
This is probably the most frequent error in the web. It is, of course,
not as simple as just setting the right header, since character
encoding must be kept consistent application-wide (database,
source/templates, etc..). I find it amazing how many people still
think character is just another word for byte... but that's for
another article...  Anyway, to declare your encoding in the headers,
use the <tt>Charset</tt> field.

<tt>Charset: iso-8859-1</tt>

## Caching
You can influence the rendering speed of your sites a lot just by
letting the clients know how to cache your files... but, maybe more
important, you will save yourself a lot of debugging headache by
knowing how to disable caching at all... I found it especially annoying
when IE8 cached AJAX call results... The following code is a bit
overkill (pragma plus Cache-Control, plus setting the expiration date
prior to the modification date), so that it works for most clients.

<pre>
Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0
Pragma: no-cache
Expires: Sun, 19 Nov 1978 05:00:00 GMT
Last-Modified: [some code to insert modification date]
</pre>   

## Mashups
The *same origin policy* can be a bummer when developing mashups. For
mozilla and chrome, there's a way for the server to let the client
know it will accept requests from other sources. It goes like this:

<pre>
Access-Control-Allow-Origin: *
Access-Control-Allow-Methods: POST, GET, OPTIONS, DELETE, PUT
</pre>   

]]
edit_ui        = ""
admin_edit_ui  = ""
breadcrumb     = ""
save_hook      = ""
author         = "admin"
creation_time  = "1297845772"
short_desc     = "Not forgetting to set the right headers for your server responses."