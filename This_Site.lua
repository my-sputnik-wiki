
title          = "This Site"
category       = ""
content        = [=[

This is the homepage of [[The Author|Joan Arnaldich]]. It's all about programming and technology.

This site runs on a custom, stripped-down version of the amazing [Sputnik]("http://sputnik.freewisdom.org/") wiki engine, which is written in the [Lua]("http://www.lua.org"). 

Why Lua? Quoting from the [Sputnik]("http://sputnik.freewisdom.org/en/Lua") project Wiki, 

> One [feature] that Sputnik exploits most aggressively is Lua's ability to serve at the same time as a programming language and a format for data storage.

That is often referred as "Code as Data" and, together with prototype-based inheritance, allows for a very neat implementation for a wiki, in which each node is actually lua code. That alone sounds just too appealing for a hacker wannabe like me...

]=]
breadcrumb     = "This Site"
