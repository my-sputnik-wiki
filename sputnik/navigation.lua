
fields         = ""
title          = "Navigation"
category       = ""
actions        = ""
config         = ""
markup_module  = ""
templates      = ""
translations   = ""
prototype      = "@Lua_Config"
permissions    = ""
html_main      = ""
html_head      = ""
html_menu      = ""
html_logo      = ""
html_search    = ""
html_page      = ""
html_content   = ""
html_body      = ""
html_footer    = ""
html_header    = ""
html_sidebar   = ""
html_meta_keywords= ""
html_meta_description= ""
redirect_destination= ""
xssfilter_allowed_tags= ""
http_cache_control= ""
http_expires   = ""
content        = [[NAVIGATION = {

   {id="blog", title="Home",
     {id="blog", title="Blog"}
   },

   {id="This Site", title="About",
    {id="This Site", title="This Site"},
    {id="The Author", title="The Author"}}

}

]]
edit_ui        = ""
admin_edit_ui  = ""
breadcrumb     = ""
save_hook      = ""
