
fields         = ""
title          = "blog/Factor file system monitor"
category       = ""
actions        = ""
config         = ""
markup_module  = ""
templates      = ""
translations   = ""
prototype      = "@BlogEntry"
permissions    = ""
html_main      = ""
html_head      = ""
html_menu      = ""
html_logo      = ""
html_search    = ""
html_page      = ""
html_content   = ""
html_body      = ""
html_footer    = ""
html_header    = ""
html_sidebar   = ""
html_meta_keywords= "factor, factorcode, file, monitor,"
html_meta_description= "Filesystem monitor implementation in the factor programming language"
redirect_destination= ""
xssfilter_allowed_tags= ""
http_cache_control= ""
http_expires   = ""
content        = [[      

Sometimes I find myself editing config scripts that won't be reloaded until the program using them is restarted, and I find the editing/stopping/starting loop really annoying, so I thought it would be nice to hack a little tool that started an application and then monitors for changes in a directory tree root, stopping and restarting the app each time a change is detected... now, Factor is the only language, AFAIK, with a portable [file system monitoring library](http://docs.factorcode.org/content/vocab-io.monitors.html). It's important to note that it doesn't fall back to active polling, but use the underlying notifications API for each platform.

In case you didn't know, Factor is a concatenative language, a sort of modern Forth on steroids with a lispy touch, created by [Slava Pestov](http://factorcode.org/slava/). Slava himself blogged about a [simple aplication](http://factor-language.blogspot.com/2008/02/simple-application-using-iomonitor-tail.html) of this library, and probably this blog post won't add much to the original article if you already know a bit of Factor, but in any case it will be yet another example of useful factor code.

If you've never played with Factor, you can download the right version for your platform [here](http://www.factorcode.org). Then visit the [getting started](http://concatenative.org/wiki/view/Factor/Learning) page. It pays off spending some time browsing the docs, because the factor team has made an effort to make the introduction as gentle as possible... 


<pre>
<span style="color: #cd5c5c;">! Vim: ts=4 :expandtab
! Copyright (C) Joan Arnaldich Bernal
! See http://factorcode.org/license.txt for BSD license.
</span><span style="color: PaleYellow;">USING:</span><span style="color: #7fffd4;"> accessors combinators fry io io.launcher io.monitors
 kernel namespaces prettyprint sequences </span><span style="color: PaleYellow;">;</span>
<span style="color: PaleYellow;">IN:</span> <span style="color: #7fffd4;">spinner</span>

<span style="color: PaleYellow;">:</span> <span style="color: #9acd32;">reset-image</span><span style="color: #cd5c5c;"> ( progname proc -- progname newproc )</span>
   kill-process dup run-detached <span style="color: PaleYellow;">;</span>

<span style="color: PaleYellow;">:</span> <span style="color: #9acd32;">change-description</span><span style="color: #cd5c5c;"> ( change -- str )</span>
    <span style="color: #9acd32;">changed&gt;&gt;</span> first {
        { +modify-file+ [ <span style="color: #ffa500;">"FILE MODIFIED"</span> ] }
        { +add-file+ [ <span style="color: #ffa500;">"FILE ADDED"</span> ] }
        { +remove-file+ [ <span style="color: #ffa500;">"FILE REMOVED"</span> ] }
        { +rename-file+ [ <span style="color: #ffa500;">"FILE RENAMED"</span> ] }
        { +rename-file-new+ [ <span style="color: #ffa500;">"FILE RENAMED"</span> ] }
        { +rename-file-old+ [ <span style="color: #ffa500;">"FILE RENAMED"</span> ] }
        [ unparse ] 
    } case <span style="color: PaleYellow;">;</span>

<span style="color: PaleYellow;">:</span> <span style="color: #9acd32;">describe-change</span><span style="color: #cd5c5c;"> ( change -- )</span>
    [ change-description ] [ <span style="color: #9acd32;">path&gt;&gt;</span> ] bi
    <span style="color: #ffa500;">" @ "</span> swap append append print nl <span style="color: PaleYellow;">;</span>

<span style="color: PaleYellow;">:</span> <span style="color: #9acd32;">print-path</span><span style="color: #cd5c5c;"> ( desc -- )</span>
    next-change describe-change flush <span style="color: PaleYellow;">;</span> <span style="color: #cd5c5c;">! 
</span>
<span style="color: PaleYellow;">:</span> <span style="color: #9acd32;">watch-loop</span><span style="color: #cd5c5c;"> ( monitor progname proc  -- )</span>
   [ dup print-path ] 2dip
   reset-image
   watch-loop <span style="color: PaleYellow;">;</span>

<span style="color: PaleYellow;">:</span> <span style="color: #9acd32;">watch-directory</span><span style="color: #cd5c5c;"> ( path progname proc -- )</span>
   [ watch-loop ] curry curry
   '[ <span style="color: PaleYellow;">t</span> _  with-monitor ] curry with-monitors <span style="color: PaleYellow;">;</span>

<span style="color: PaleYellow;">:</span> <span style="color: #9acd32;">start-spinning</span><span style="color: #cd5c5c;"> ( path progname -- )</span>
   dup run-detached watch-directory <span style="color: PaleYellow;">;</span>

<span style="color: PaleYellow;">:</span> <span style="color: #9acd32;">main</span><span style="color: #cd5c5c;"> ( -- )</span>
   <span style="color: #ffa500;">"path"</span> get-global
   <span style="color: #ffa500;">"exec"</span> get-global
   start-spinning <span style="color: PaleYellow;">;</span>

<span style="color: PaleYellow;">MAIN:</span> <span style="color: #7fffd4;">main</span>
</pre>

There's really not much to comment on this code. We start by reading
the two params from globals (MAIN is the only place where I allow
myself to use globals), and then push them onto the stack to call the
function `start-spinning`, that starts the process for the first time
and then calls `watch-directory`, which first builds a specialized
watch loop through currying... I guess this is the concatenative
equivalent of a HOF (ain't concatenative nice?) and then submits this
newly created function to `with-monitors`. The loop, as seen in this
function, just blocks for an event (`next-change` is a blocking call),
prints a readable message, and keeps looping.

This will allow run it at the command-line like this:

<pre>
factor -exec=<em>program-to-reset</em> -path=<em>path-to-monitor</em>
</pre>

Voilà! That was it! 

]]
edit_ui        = ""
admin_edit_ui  = ""
breadcrumb     = ""
save_hook      = ""
author         = "admin"
creation_time  = "1296313923"
short_desc     = ""
